import unittest
import hello

class Testhello(unittest.TestCase):
    def test_add(self):
        h=hello.Hello()
        res=h.add(2,3)
        self.assertEqual(res, 5)

if __name__ == "__main__":
    unittest.main()